package busyflights.impl;

import busyflights.domain.mapper.CrazyAirRequestMapper;
import busyflights.domain.mapper.CrazyAirResponseMapper;
import busyflights.domain.mapper.ToughJetRequestMapper;
import busyflights.domain.mapper.ToughJetResponseMapper;
import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.CrazyAirRequest;
import busyflights.domain.request.api.ToughJetRequest;
import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.CrazyAirResponse;
import busyflights.domain.response.api.ToughJetResponse;
import busyflights.service.ProviderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServiceImplTest {

    @Mock
    private ProviderService providerService;

    @InjectMocks
    private FlightSearchServiceImpl service;

    private static final LocalDateTime DEPT = LocalDateTime.of(2017, 1, 1, 12, 12, 12);

    private static final LocalDateTime ARRI = LocalDateTime.of(2017,1,1, 23,12,12);

    @Test
    public void canAggregateResults() {
        final FlightSearchRequest flightSearchRequest = new FlightSearchRequest("ORI", "DES", DEPT, ARRI, 2);

        final ToughJetRequest toughJetRequest = new ToughJetRequestMapper().apply(flightSearchRequest);
        final CrazyAirRequest crazyAirRequest = new CrazyAirRequestMapper().apply(flightSearchRequest);

        final FlightSearchResponse flightSearchResponseTough = new ToughJetResponseMapper().apply(toughJetResponse);
        final FlightSearchResponse flightSearchResponseCrazy = new CrazyAirResponseMapper().apply(crazyAirResponse);

        when(providerService.retrieve(toughJetRequest)).thenReturn(toughJetResponse);
        when(providerService.retrieve(crazyAirRequest)).thenReturn(crazyAirResponse);

        List<FlightSearchResponse> actual = service.search(flightSearchRequest);
        List<FlightSearchResponse> expected = Arrays.asList(flightSearchResponseCrazy, flightSearchResponseTough);

        // Order of Price Desc
        assertThat(actual, is(expected));
    }

    private static final ToughJetResponse toughJetResponse = new ToughJetResponse("carrier", 122.2245D, 01.00D,
                                                                                  10.00D, "HTW", "BGK", 1,
                                                                                  1, 2012, 2, 1, 2012);

    private static final CrazyAirResponse crazyAirResponse = new CrazyAirResponse("airline", 34.00D, "ECO",
                                                                                  "LTN", "BKC",
                                                                                  "03-20-2017 12:00:00", "04-20-2017 12:00:00");

}
