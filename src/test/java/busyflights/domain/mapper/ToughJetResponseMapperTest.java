package busyflights.domain.mapper;

import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.ToughJetResponse;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


public class ToughJetResponseMapperTest {

    private final ToughJetResponseMapper mapper = new ToughJetResponseMapper();

    private static final LocalDateTime DPT = LocalDateTime.of(2012, 1, 1, 0,0,0);
    private static final LocalDateTime ARR = LocalDateTime.of(2012, 1, 2, 0,0,0);

    @Test
    public void canMapFull() {
        ToughJetResponse apiResponse = new ToughJetResponse("carrier", 122.2245D, 01.00D,
                10.00D, "HTW", "BGK", 1,
                1, 2012, 2, 1, 2012);

        final FlightSearchResponse response = mapper.apply(apiResponse);

        assertThat(response.getAirline(), is("carrier"));
        assertThat(response.getSupplier(), is(apiResponse.getName().getName()));
        assertThat(response.getFare(), is(134.45D));
        assertThat(response.getDepartureAirportCode(), is("HTW"));
        assertThat(response.getDestinationAirportCode(), is("BGK"));
        assertThat(response.getDepartureDate(), is(DPT));
        assertThat(response.getArrivalDate(), is(ARR));
    }

}
