package busyflights.domain.mapper;


import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.ToughJetRequest;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ToughJetRequestMapperTest {

    private static final LocalDateTime DEPT = LocalDateTime.of(2017,1,1, 12,12,12);

    private static final LocalDateTime ARRI = LocalDateTime.of(2017,1,1, 23,12,12);

    private final ToughJetRequestMapper requestMapper = new ToughJetRequestMapper();

    @Test
    public void canMapFullRequest() {
        final FlightSearchRequest flightSearchRequest = new FlightSearchRequest("ORI", "DES", DEPT, ARRI, 2);
        final ToughJetRequest toughJetRequest = requestMapper.apply(flightSearchRequest);

        assertThat(toughJetRequest.getFrom(), is("ORI"));
        assertThat(toughJetRequest.getTo(), is("DES"));
        assertThat(toughJetRequest.getAdultTotal(), is(flightSearchRequest.getNumberOfPassengers()));
        assertThat(toughJetRequest.getAdultTotal(), is(flightSearchRequest.getNumberOfPassengers()));

        assertDate(toughJetRequest.getDepartureDay(), toughJetRequest.getDepartureMonth(), toughJetRequest.getDepartureYear(), DEPT);
        assertDate(toughJetRequest.getReturnDay(), toughJetRequest.getReturnMonth(), toughJetRequest.getReturnYear(), ARRI);
    }

    private static void assertDate(int day, int month, int year, LocalDateTime date) {
        assertThat(day, is(date.getDayOfMonth()));
        assertThat(month, is(date.getMonthValue()));
        assertThat(year, is(date.getYear()));
    }

}
