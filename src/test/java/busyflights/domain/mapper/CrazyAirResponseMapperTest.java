package busyflights.domain.mapper;

import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.CrazyAirResponse;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


public class CrazyAirResponseMapperTest {

    private final static DateTimeFormatter DF = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");

    private final CrazyAirResponseMapper mapper = new CrazyAirResponseMapper();

    @Test
    public void testNormalMapping() {
        CrazyAirResponse crazyAirResponse = new CrazyAirResponse("airline", 34.00D, "ECO",
                                                                "LTN", "BKC",
                                                                "03-20-2017 12:00:00", "04-20-2017 12:00:00");

        final FlightSearchResponse resp = mapper.apply(crazyAirResponse);

        assertThat(resp.getAirline(), is("airline"));
        assertThat(resp.getFare(), is(34.00D));
        assertThat(resp.getDepartureAirportCode(), is("LTN"));
        assertThat(resp.getDestinationAirportCode(), is("BKC"));
        assertThat(resp.getDepartureDate(), is(LocalDateTime.from(DF.parse("03-20-2017 12:00:00"))));
        assertThat(resp.getArrivalDate(), is(LocalDateTime.from(DF.parse("04-20-2017 12:00:00"))));
    }

}