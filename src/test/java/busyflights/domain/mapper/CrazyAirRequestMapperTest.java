package busyflights.domain.mapper;

import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.CrazyAirRequest;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


public class CrazyAirRequestMapperTest {

    private static final LocalDateTime DEPT = LocalDateTime.of(2017,1,1, 12,12,12);

    private static final LocalDateTime ARRI = LocalDateTime.of(2017,2,1, 23,12,12);

    private final CrazyAirRequestMapper mapper = new CrazyAirRequestMapper();

    @Test
    public void canMapFull() {
        final FlightSearchRequest flightSearchRequest = new FlightSearchRequest("ORI", "DES", DEPT, ARRI, 2);
        final CrazyAirRequest request = mapper.apply(flightSearchRequest);

        assertThat(request.getOrigin(), is("ORI"));
        assertThat(request.getDestination(), is("DES"));
        assertThat(request.getDepartureDate(), is("01-01-2017"));
        assertThat(request.getReturnDate(), is("02-01-2017"));
        assertThat(request.getPassengerTotal(), is(2));


    }

}