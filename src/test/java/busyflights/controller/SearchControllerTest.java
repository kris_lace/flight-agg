package busyflights.controller;

import busyflights.Application;
import busyflights.service.FlightSearchService;
import busyflights.service.ProviderService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class SearchControllerTest {

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                        MediaType.APPLICATION_JSON.getSubtype(),
                                                        Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private SearchController searchController;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FlightSearchService flightSearchService;

    @Autowired
    private ProviderService providerService;

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void fishy() throws Exception {
        final ResultActions actions = mockMvc.perform(get("/flights/fishy"))
                                             .andExpect(status().isOk())
                                             .andExpect(content().string("><)))'> Fishy says hello there ;)"));
    }

    @Test
    @Ignore("TODO - Need to Mock API's")
    public void search() throws Exception {
        mockMvc.perform(post("/flights/search")
                        .contentType(contentType)
                        .content(simpleRequest))
                        .andExpect(status().isCreated());
    }

    private static final String simpleRequest = "{ \"origin\":\"LTN\", \"destination\":\"LHW\", \"departureDate\":\"2017-02-13T12:05:45\", \"returnDate\":\"2017-02-14T12:05:45\", \"numberOfPassengers\":2 }";

}
