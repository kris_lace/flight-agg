package busyflights.impl;

import busyflights.domain.FlightSupplier;
import busyflights.domain.mapper.CrazyAirRequestMapper;
import busyflights.domain.mapper.CrazyAirResponseMapper;
import busyflights.domain.mapper.ToughJetRequestMapper;
import busyflights.domain.mapper.ToughJetResponseMapper;
import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.ApiRequest;
import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.ApiResponse;
import busyflights.service.FlightSearchService;
import busyflights.service.ProviderService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class FlightSearchServiceImpl implements FlightSearchService {

    private final Comparator<? super FlightSearchResponse> LOWEST_COST = Comparator.comparingDouble(FlightSearchResponse::getFare);

    private final Map<FlightSupplier, Function<ApiResponse, FlightSearchResponse>> providerResponseMapper
        = ImmutableMap.of(FlightSupplier.TOUGH, new ToughJetResponseMapper(),
                          FlightSupplier.CRAZY, new CrazyAirResponseMapper());

    private final List<Function<FlightSearchRequest, ApiRequest>> requestMappers
        = Arrays.asList(new CrazyAirRequestMapper(),
                        new ToughJetRequestMapper());

    private final ProviderService providerService;

    @Autowired
    public FlightSearchServiceImpl(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public List<FlightSearchResponse> search(FlightSearchRequest flightSearchRequest) {
        return requestMappers.stream()
                             .map(mapper -> mapper.apply(flightSearchRequest))
                             .map(providerService::retrieve)
                             .map(response -> providerResponseMapper.get(response.getName()).apply(response))
                             .sorted(LOWEST_COST)
                             .collect(Collectors.toList());
    }

}
