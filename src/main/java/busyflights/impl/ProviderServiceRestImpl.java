package busyflights.impl;

import busyflights.domain.FlightSupplier;
import busyflights.domain.request.api.ApiRequest;
import busyflights.domain.response.api.ApiResponse;
import busyflights.domain.response.api.CrazyAirResponse;
import busyflights.domain.response.api.ToughJetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import busyflights.service.ProviderService;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class ProviderServiceRestImpl implements ProviderService {

    private final Map<FlightSupplier, Function<ApiRequest, ApiResponse>> providerMap;

    private final RestTemplate restTemplate;

    @Autowired
    public ProviderServiceRestImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.providerMap = new HashMap<>();
        providerMap.put(FlightSupplier.CRAZY, this::retrieveCrazyAir);
        providerMap.put(FlightSupplier.TOUGH, this::retrieveToughJet);
    }

    @Override
    public ApiResponse retrieve(ApiRequest apiRequest) {
        return providerMap.get(apiRequest.getApiName())
                          .apply(apiRequest);
    }

    private ApiResponse retrieveCrazyAir(ApiRequest apiRequest) {
        return restTemplate.postForObject("http://crazy-url.com/search", apiRequest, CrazyAirResponse.class);
    }

    private ApiResponse retrieveToughJet(ApiRequest apiRequest) {
        return restTemplate.postForObject("http://tough-jet.com/search", apiRequest, ToughJetResponse.class);
    }

}
