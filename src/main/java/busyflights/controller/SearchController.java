package busyflights.controller;

import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.response.FlightSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import busyflights.service.FlightSearchService;

import java.util.List;

@RestController
@RequestMapping("/flights")
@EnableAutoConfiguration
public class SearchController {

    private final FlightSearchService flightSearchService;

    @Autowired
    public SearchController(FlightSearchService flightSearchService) {
        this.flightSearchService = flightSearchService;
    }

    @RequestMapping(value = "/fishy")
    public String hello() {
        return "><)))'> Fishy says hello there ;)";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public List<FlightSearchResponse> search(@RequestBody FlightSearchRequest request) {
        return flightSearchService.search(request);
    }

}
