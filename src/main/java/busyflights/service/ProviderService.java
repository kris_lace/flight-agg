package busyflights.service;

import busyflights.domain.request.api.ApiRequest;
import busyflights.domain.response.api.ApiResponse;
import org.springframework.stereotype.Service;

public interface ProviderService {

    ApiResponse retrieve(ApiRequest apiRequest);

}
