package busyflights.service;


import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.response.FlightSearchResponse;

import java.util.List;

public interface FlightSearchService {

    List<FlightSearchResponse> search(FlightSearchRequest flightSearchRequest);

}
