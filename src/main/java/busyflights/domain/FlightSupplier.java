package busyflights.domain;

public enum FlightSupplier {

    CRAZY("CrazyAir"),

    TOUGH("ToughJet");

    private final String name;

    FlightSupplier(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
