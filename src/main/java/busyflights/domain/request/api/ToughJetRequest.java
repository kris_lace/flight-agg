package busyflights.domain.request.api;


import busyflights.domain.FlightSupplier;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Objects;

@JsonInclude
public class ToughJetRequest implements ApiRequest {

    private final String from;

    private final String to;

    private final int departureDay;

    private final int departureMonth;

    private final int departureYear;

    private final int returnDay;

    private final int returnMonth;

    private final int returnYear;

    private final int adultTotal;

    private ToughJetRequest(String from, String to, int departureDay, int departureMonth, int departureYear, int returnDay, int returnMonth, int returnYear, int adultTotal) {
        this.from = from;
        this.to = to;
        this.departureDay = departureDay;
        this.departureMonth = departureMonth;
        this.departureYear = departureYear;
        this.returnDay = returnDay;
        this.returnMonth = returnMonth;
        this.returnYear = returnYear;
        this.adultTotal = adultTotal;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public int getDepartureDay() {
        return departureDay;
    }

    public int getDepartureMonth() {
        return departureMonth;
    }

    public int getDepartureYear() {
        return departureYear;
    }

    public int getReturnDay() {
        return returnDay;
    }

    public int getReturnMonth() {
        return returnMonth;
    }

    public int getReturnYear() {
        return returnYear;
    }

    public int getAdultTotal() {
        return adultTotal;
    }

    @Override
    public FlightSupplier getApiName() {
        return FlightSupplier.TOUGH;
    }

    public static class Builder {

        private String from;

        private String to;

        private int departureDay;

        private int departureMonth;

        private int departureYear;

        private int returnDay;

        private int returnMonth;

        private int returnYear;

        private int adultTotal;

        public Builder from(String from) {
            this.from = from;
            return this;
        }

        public Builder to(String to) {
            this.to = to;
            return this;
        }

        public Builder departureDay(int departureDay) {
            this.departureDay = departureDay;
            return this;
        }
        public Builder departureMonth(int departureMonth) {
            this.departureMonth = departureMonth;
            return this;
        }
        public Builder departureYear(int departureYear) {
            this.departureYear = departureYear;
            return this;
        }
        public Builder returnDay(int returnDay) {
            this.returnDay = returnDay;
            return this;
        }
        public Builder returnMonth(int returnMonth) {
            this.returnMonth = returnMonth;
            return this;
        }
        public Builder returnYear(int returnYear) {
            this.returnYear = returnYear;
            return this;
        }
        public Builder adultTotal(int adultTotal) {
            this.adultTotal = adultTotal;
            return this;
        }

        public ToughJetRequest build() {
            return new ToughJetRequest(
                    this.from,
                    this.to,
                    this.departureDay,
                    this.departureMonth,
                    this.departureYear,
                    this.returnDay,
                    this.returnMonth,
                    this.returnYear,
                    this.adultTotal);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ToughJetRequest that = (ToughJetRequest) obj;
        return Objects.equals(this.from, that.from) &&
               Objects.equals(this.to, that.to) &&
               Objects.equals(this.departureDay, that.departureDay) &&
               Objects.equals(this.departureMonth, that.departureMonth) &&
               Objects.equals(this.departureYear, that.departureYear) &&
               Objects.equals(this.returnDay, that.returnDay) &&
               Objects.equals(this.returnMonth, that.returnMonth) &&
               Objects.equals(this.returnYear, that.returnYear) &&
               Objects.equals(this.adultTotal, that.adultTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.from, this.to, this.departureDay, this.departureMonth, this.departureYear,
                            this.returnDay, this.returnMonth, this.returnYear, this.adultTotal);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
