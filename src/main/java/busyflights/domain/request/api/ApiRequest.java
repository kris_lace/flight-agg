package busyflights.domain.request.api;


import busyflights.domain.FlightSupplier;

public interface ApiRequest {

    FlightSupplier getApiName();

}
