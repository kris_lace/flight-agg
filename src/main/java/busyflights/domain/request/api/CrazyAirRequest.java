package busyflights.domain.request.api;


import busyflights.domain.FlightSupplier;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Objects;

@JsonInclude
public class CrazyAirRequest implements ApiRequest {

    private final String origin;

    private final String destination;

    private final String departureDate;

    private final String returnDate;

    private final int passengerTotal;

    @JsonCreator
    public CrazyAirRequest(@JsonProperty("origin") String origin,
                           @JsonProperty("destination") String destination,
                           @JsonProperty("departureDate") String departureDate,
                           @JsonProperty("returnDate") String returnDate,
                           @JsonProperty("passengerTotal") int passengerTotal) {

        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.passengerTotal = passengerTotal;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public int getPassengerTotal() {
        return passengerTotal;
    }

    @Override
    public FlightSupplier getApiName() {
        return FlightSupplier.CRAZY;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final CrazyAirRequest that = (CrazyAirRequest) obj;
        return Objects.equals(this.origin, that.origin) &&
               Objects.equals(this.destination, that.destination) &&
               Objects.equals(this.departureDate, that.departureDate) &&
               Objects.equals(this.returnDate, that.returnDate) &&
               Objects.equals(this.passengerTotal, that.passengerTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.origin, this.destination, this.departureDate, this.returnDate, this.passengerTotal);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
