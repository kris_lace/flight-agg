package busyflights.domain.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonInclude
public class FlightSearchRequest {

    private final String origin;

    private final String destination;

    private final LocalDateTime departureDate;

    private final LocalDateTime returnDate;

    private final int numberOfPassengers;

    @JsonCreator
    public FlightSearchRequest(@JsonProperty("origin") String origin,
                               @JsonProperty("destination") String destination,
                               @JsonProperty("departureDate") LocalDateTime departureDate,
                               @JsonProperty("returnDate") LocalDateTime returnDate,
                               @JsonProperty("numberOfPassengers") int numberOfPassengers) {

        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.numberOfPassengers = numberOfPassengers;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final FlightSearchRequest that = (FlightSearchRequest) obj;
        return Objects.equals(this.origin, that.origin) &&
               Objects.equals(this.destination, that.destination) &&
               Objects.equals(this.numberOfPassengers, that.numberOfPassengers) &&
               Objects.equals(this.departureDate, that.departureDate) &&
               Objects.equals(this.returnDate, that.returnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.origin, this.destination, this.numberOfPassengers, this.departureDate, this.returnDate);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
