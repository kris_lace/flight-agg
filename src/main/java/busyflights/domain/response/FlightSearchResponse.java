package busyflights.domain.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonInclude
public class FlightSearchResponse {

    private final String airline;

    private final String supplier;

    private final double fare;

    private final String departureAirportCode;

    private final String destinationAirportCode;

    private final LocalDateTime departureDate;

    private final LocalDateTime arrivalDate;

    public FlightSearchResponse(String airline, String supplier, double fare, String departureAirportCode, String destinationAirportCode, LocalDateTime departureDate, LocalDateTime arrivalDate) {
        this.airline = airline;
        this.supplier = supplier;
        this.fare = fare;
        this.departureAirportCode = departureAirportCode;
        this.destinationAirportCode = destinationAirportCode;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public String getAirline() {
        return airline;
    }

    public String getSupplier() {
        return supplier;
    }

    public double getFare() {
        return fare;
    }

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final FlightSearchResponse that = (FlightSearchResponse) obj;
        return Objects.equals(this.airline, that.airline) &&
               Objects.equals(this.supplier, that.supplier) &&
               Objects.equals(this.fare, that.fare) &&
               Objects.equals(this.departureAirportCode, that.departureAirportCode) &&
               Objects.equals(this.destinationAirportCode, that.destinationAirportCode) &&
               Objects.equals(this.departureDate, that.departureDate) &&
               Objects.equals(this.arrivalDate, that.arrivalDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.airline, this.supplier, this.fare, this.departureAirportCode, this.destinationAirportCode,
                            this.departureDate, this.arrivalDate);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
