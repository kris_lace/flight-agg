package busyflights.domain.response.api;


import busyflights.domain.FlightSupplier;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Objects;

@JsonInclude
public class CrazyAirResponse implements ApiResponse {

    private final String airline;

    private final double price;

    // TODO enum
    private final String cabinClass;

    private final String departureAirportCode;

    private final String destinationAirportCode;

    private final String departureDate;

    private final String arrivalDate;

    @JsonCreator
    public CrazyAirResponse(@JsonProperty("airline") String airline,
                            @JsonProperty("price") double price,
                            @JsonProperty("cabinClass") String cabinClass,
                            @JsonProperty("departureAirportCode") String departureAirportCode,
                            @JsonProperty("destinationAirportCode") String destinationAirportCode,
                            @JsonProperty("departureDate") String departureDate,
                            @JsonProperty("arrivalDate") String arrivalDate) {

        this.airline = airline;
        this.price = price;
        this.cabinClass = cabinClass;
        this.departureAirportCode = departureAirportCode;
        this.destinationAirportCode = destinationAirportCode;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public String getAirline() {
        return airline;
    }

    public double getPrice() {
        return price;
    }

    public String getCabinClass() {
        return cabinClass;
    }

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public FlightSupplier getName() {
        return FlightSupplier.CRAZY;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final CrazyAirResponse that = (CrazyAirResponse) obj;
        return Objects.equals(this.airline, that.airline) &&
               Objects.equals(this.price, that.price) &&
               Objects.equals(this.cabinClass, that.cabinClass) &&
               Objects.equals(this.departureDate, that.departureDate) &&
               Objects.equals(this.destinationAirportCode, that.destinationAirportCode) &&
               Objects.equals(this.arrivalDate, that.arrivalDate) &&
               Objects.equals(this.departureAirportCode, that.departureAirportCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.airline, this.price, this.cabinClass, this.departureDate, this.departureAirportCode,
                           this.destinationAirportCode, arrivalDate);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
