package busyflights.domain.response.api;

import busyflights.domain.FlightSupplier;

public interface ApiResponse {

    FlightSupplier getName();

}
