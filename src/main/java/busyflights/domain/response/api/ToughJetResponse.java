package busyflights.domain.response.api;

import busyflights.domain.FlightSupplier;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Objects;

@JsonInclude
public class ToughJetResponse implements ApiResponse {

    private final String carrier;

    private final double basePrice;

    private final double tax;

    private final double percentageDiscount;

    private final String departureAirportName;

    private final String arrivalAirportName;

    private final int departureDay;

    private final int departureMonth;

    private final int departureYear;

    private final int returnDay;

    private final int returnMonth;

    private final int returnYear;

    public ToughJetResponse(String carrier, double basePrice, double tax, double percentageDiscount, String departureAirportName, String arrivalAirportName, int departureDay, int departureMonth, int departureYear, int returnDay, int returnMonth, int returnYear) {
        this.carrier = carrier;
        this.basePrice = basePrice;
        this.tax = tax;
        this.percentageDiscount = percentageDiscount;
        this.departureAirportName = departureAirportName;
        this.arrivalAirportName = arrivalAirportName;
        this.departureDay = departureDay;
        this.departureMonth = departureMonth;
        this.departureYear = departureYear;
        this.returnDay = returnDay;
        this.returnMonth = returnMonth;
        this.returnYear = returnYear;
    }

    public String getCarrier() {
        return carrier;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public double getTax() {
        return basePrice;
    }

    public double getPercentageDiscount() {
        return percentageDiscount;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public int getDepartureDay() {
        return departureDay;
    }

    public int getDepartureMonth() {
        return departureMonth;
    }

    public int getDepartureYear() {
        return departureYear;
    }

    public int getReturnDay() {
        return returnDay;
    }

    public int getReturnMonth() {
        return returnMonth;
    }

    public int getReturnYear() {
        return returnYear;
    }

    public FlightSupplier getName() {
        return FlightSupplier.TOUGH;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ToughJetResponse that = (ToughJetResponse) obj;
        return Objects.equals(this.carrier, that.carrier) &&
               Objects.equals(this.basePrice, that.basePrice) &&
               Objects.equals(this.basePrice, that.basePrice) &&
               Objects.equals(this.percentageDiscount, that.percentageDiscount) &&
               Objects.equals(this.departureAirportName, that.departureAirportName) &&
               Objects.equals(this.arrivalAirportName, that.arrivalAirportName) &&
               Objects.equals(this.returnYear, that.returnYear) &&
               Objects.equals(this.returnMonth, that.returnMonth) &&
               Objects.equals(this.returnDay, that.returnDay) &&
               Objects.equals(this.departureYear, that.departureYear) &&
               Objects.equals(this.departureMonth, that.departureMonth) &&
               Objects.equals(this.departureDay, that.departureDay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.carrier, this.basePrice, this.basePrice, this.percentageDiscount, this.departureAirportName,
                            this.arrivalAirportName, this.departureMonth, this.departureYear, this.returnDay, this.departureDay,
                            this.returnMonth, this.returnYear);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
