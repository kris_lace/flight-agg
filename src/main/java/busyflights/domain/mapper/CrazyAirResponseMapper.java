package busyflights.domain.mapper;

import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.ApiResponse;
import busyflights.domain.response.api.CrazyAirResponse;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class CrazyAirResponseMapper implements Function<ApiResponse, FlightSearchResponse> {

    private final static DateTimeFormatter DF = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");

    @Override
    public FlightSearchResponse apply(ApiResponse apiResponse) {
        final CrazyAirResponse crazyAirResponse = (CrazyAirResponse) apiResponse;
        return new FlightSearchResponse(crazyAirResponse.getAirline(),
                                                         crazyAirResponse.getName().getName(),
                                                         crazyAirResponse.getPrice(),
                                                         crazyAirResponse.getDepartureAirportCode(),
                                                         crazyAirResponse.getDestinationAirportCode(),
                                                         LocalDateTime.from(DF.parse(crazyAirResponse.getDepartureDate())),
                                                         LocalDateTime.from(DF.parse(crazyAirResponse.getArrivalDate())));
    }

}
