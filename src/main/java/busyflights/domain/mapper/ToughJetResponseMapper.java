package busyflights.domain.mapper;

import busyflights.domain.response.FlightSearchResponse;
import busyflights.domain.response.api.ApiResponse;
import busyflights.domain.response.api.ToughJetResponse;
import java.time.LocalDateTime;
import java.util.function.Function;

public class ToughJetResponseMapper implements Function<ApiResponse, FlightSearchResponse> {

    @Override
    public FlightSearchResponse apply(ApiResponse apiResponse) {
        ToughJetResponse toughJetResponse = (ToughJetResponse) apiResponse;

        double price = round(toughJetResponse.getBasePrice()
                     + toughJetResponse.getTax()
                     * (toughJetResponse.getPercentageDiscount() / 100));

        final LocalDateTime returnTime = LocalDateTime.of(toughJetResponse.getReturnYear(),
                toughJetResponse.getReturnMonth(), toughJetResponse.getReturnDay(), 0,0,0);

        final LocalDateTime deptTime = LocalDateTime.of(toughJetResponse.getDepartureYear(),
                toughJetResponse.getDepartureMonth(), toughJetResponse.getDepartureDay(), 0,0,0);

        return new FlightSearchResponse(toughJetResponse.getCarrier(),
                toughJetResponse.getName().getName(),
                price,
                toughJetResponse.getDepartureAirportName(),
                toughJetResponse.getArrivalAirportName(),
                deptTime,
                returnTime);
    }

    private static double round(double n) {
        return (double) Math.round(n * 100) / 100;
    }
}
