package busyflights.domain.mapper;

import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.ApiRequest;
import busyflights.domain.request.api.ToughJetRequest;

import java.util.function.Function;


public class ToughJetRequestMapper implements Function<FlightSearchRequest, ApiRequest> {

    @Override
    public ToughJetRequest apply(FlightSearchRequest flightSearchRequest) {
        return new ToughJetRequest.Builder()
                                  .to(flightSearchRequest.getDestination())
                                  .from(flightSearchRequest.getOrigin())
                                  .departureDay(flightSearchRequest.getDepartureDate().getDayOfMonth())
                                  .departureMonth(flightSearchRequest.getDepartureDate().getMonthValue())
                                  .departureYear(flightSearchRequest.getDepartureDate().getYear())
                                  .returnDay(flightSearchRequest.getReturnDate().getDayOfMonth())
                                  .returnMonth(flightSearchRequest.getReturnDate().getMonthValue())
                                  .returnYear(flightSearchRequest.getReturnDate().getYear())
                                  .adultTotal(flightSearchRequest.getNumberOfPassengers())
                                  .build();
    }
}
