package busyflights.domain.mapper;

import busyflights.domain.request.FlightSearchRequest;
import busyflights.domain.request.api.ApiRequest;
import busyflights.domain.request.api.CrazyAirRequest;

import java.time.format.DateTimeFormatter;
import java.util.function.Function;


public class CrazyAirRequestMapper implements Function<FlightSearchRequest, ApiRequest> {

    private static final DateTimeFormatter DF = DateTimeFormatter.ofPattern("MM-dd-yyyy");

    @Override
    public CrazyAirRequest apply(FlightSearchRequest flightSearchRequest) {
        return new CrazyAirRequest(flightSearchRequest.getOrigin(),
                                   flightSearchRequest.getDestination(),
                                   flightSearchRequest.getDepartureDate().format(DF),
                                   flightSearchRequest.getReturnDate().format(DF),
                                   flightSearchRequest.getNumberOfPassengers());
    }
}
